import os
from flask import Flask, render_template, request
from flask_mysqldb import MySQL
import yaml
template_dir = os.path.abspath('C:/Users/Abhinav/Desktop/flaskapp/templates/')
app = Flask(__name__, template_folder = template_dir)

db = yaml.load(open('db.yaml'))
app.config['MYSQL_HOST'] = db['mysql_host']
app.config['MYSQL_USER'] = db['mysql_user']
app.config['MYSQL_PASSWORD'] = db['mysql_password']
app.config['MYSQL_DB'] = db['mysql_db']
mysql = MySQL(app)

@app.route('/employee', methods = ['GET', 'POST'])
def index():
    if request.method == 'POST':
        userDetails = request.form  
        EMPLOYEE_ID = userDetails['EMPLOYEE_ID']
        NAME = userDetails['NAME']
        EMAIL = userDetails['EMAIL']
        CONTACT = userDetails['CONTACT']
        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO EMPLOYEE_DETAILS(EMPLOYEE_ID, NAME, EMAIL, CONTACT) VALUES(%s, %s, %s, %s)", (EMPLOYEE_ID, NAME, EMAIL, CONTACT))
        mysql.connection.commit()
        cur.close()
        return 'Employee Details has been updated'
    return render_template('index.html')


@app.route('/job', methods = ['GET', 'POST'])
def job():
    if request.method == 'POST':
        userDetails0 = request.form  
        EMPLOYEE_ID = userDetails0['EMPLOYEE_ID']
        JOB_ID = userDetails0['JOB_ID']
        JOB_DESC = userDetails0['JOB_DESC']
        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO JOB_DETAILS(EMPLOYEE_ID, JOB_ID, JOB_DESC) VALUES(%s, %s, %s)", (EMPLOYEE_ID, JOB_ID, JOB_DESC))
        mysql.connection.commit()
        cur.close()
        return 'Job Details has been updated'
    return render_template('index1.html')


@app.route('/desc')
def desc():
    cur = mysql.connection.cursor()
    result = cur.execute("SELECT T1.EMPLOYEE_ID, T1.NAME, T2.JOB_DESC FROM EMPLOYEE_DETAILS T1, JOB_DETAILS T2 JOIN EMPLOYEE_DETAILS t ON t.EMPLOYEE_ID = T2.EMPLOYEE_ID ")
    if result > 0 :
        userDetails1 = cur.fetchall()
        return render_template('desc.html', userDetails1 = userDetails1)

@app.route('/email', methods = ['GET', 'POST'])
def email():
    if request.method == 'POST':
        userDetailse = request.form
        NAME1 = userDetailse['NAME']
    return render_template('email1.html')
    
    if request.method == 'GET':
        language = request.form.get('NAME')
        cur = mysql.connection.cursor()
    result = cur.execute("SELECT * from EMPLOYEE_DETAILS WHERE EMAIL LIKE '"" + NAME' ; ")
    if result > 0 :
        userDetails2 = cur.fetchall()
        return render_template('email.html', userDetails2 = userDetails2)



# @app.route('/upd')
# def upd():
#     cur = mysql.connection.cursor()
#     result = cur.execute("SELECT * from EMPLOYEE_DETAILS WHERE EMAIL LIKE '"" + NAME1' ; ")
#     if result > 0 :
#         userDetails2 = cur.fetchall()
#         return render_template('email.html', userDetails2 = userDetails2)


               

# @app.route('/email')
# def email():
#     cur = mysql.connection.cursor()
#     result = cur.execute("SELECT * from EMPLOYEE_DETAILS WHERE EMAIL LIKE '%so%' ; ")
#     if result > 0 :
#         userDetails2 = cur.fetchall()
#         return render_template('email.html', userDetails2 = userDetails2)

@app.route('/delete')
def delete():
    cur = mysql.connection.cursor()
    result1 = cur.execute("DELETE from EMPLOYEE_DETAILS WHERE NAME = 'Abhinav'; ")
    result = cur.execute("SELECT * from EMPLOYEE_DETAILS")
    if result > 0 :
        userDetails3 = cur.fetchall()
        return render_template('delete.html', userDetails3 = userDetails3)

if __name__ == '__main__':
    app.run(debug = True)


